/* common.h -- Common utility functions for the project. */
#ifndef COMMON_H
#define COMMON_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Returns the length of a file. 
 * NOTE: Currently changes the file's position.
 */
int flen(FILE *f);

/* Fully tokenizes a string. Best used for path separation.
 * Destroys the source string.
 */
char **tokenize(char *src, char *tokstr, int *numtoks);
#endif
