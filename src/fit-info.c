/* fit-info.c -- Prints out filesystem information for a disk image. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <getopt.h>

#include "common.h"
#include "fatimg.h"

void print_usage()
{
    printf("Usage: fit-info <image name>\n");
    printf("Prints information about a disk image.\n");
}

void prnstrn(uint8_t *ptr, int len)
{
    int i;

    for(i=0;i<len;i++)
    {
        printf("%c", ptr[i]);
    }
}
void print_info(char *filename)
{
    imgdesc_t img;
    int i;
    
    img=openimg((const char*) filename);

    if(img.f==NULL)
    {
        printf("Couldn't open image file: %s\n", filename);
        return;
    }

    printf("Image information:\n");
    printf("Label: ");
    prnstrn(img.bootsector->vol_label, 11);

    printf("\nOEM Label: ");
    prnstrn(img.bootsector->oem_label, 8);

    printf("\nFormat string: ");
    prnstrn(img.bootsector->fstype_str, 8);

    printf("\nMedia type:          %x ", img.bootsector->media_desc);

    if(img.bootsector->media_desc==0xF0)
    {
        printf("(1.44MB or 2.88MB Diskette)");
    }

    else if(img.bootsector->media_desc==0xF8)
    {
        printf("(hard disk)");
    }

    else
    {
        printf("(?)");
    }

    printf("\nFAT size (sects):    %d\n", img.bootsector->sects_per_fat);
    printf("Root directory size: %d entries\n", img.bootsector->root_dir_size);
    printf("Sector size:         %d\n", img.sect_size);
    printf("Sectors per cluster: %d\n", img.bootsector->sects_per_clstr);
    printf("Reserved sectors:    %d\n", img.bootsector->rsrvd_sects);
    printf("16-bit size:         %d sectors\n", img.bootsector->num_sectors);
    printf("32-bit size:         %d sectors\n", img.bootsector->large_num_sectors);
    printf("Sectors per track:   %d\n", img.bootsector->sects_per_track);
    printf("Heads per cylinder:  %d\n", img.bootsector->num_heads);


    closeimg(&img);
}

int main(int argc, char **argv)
{
    char *name;

    if(argc<2)
    {
        print_usage();
        return 0;
    }
    
    name=argv[1];

    print_info(name);

    return 0;
}
