/* fatimg.c -- Implementation for FAT image operations. */

#include "fatimg.h"

geometry_t calculate(int rawnum, int spc, int hpc)
{
    geometry_t geo;

    if(spc==0)
    {
        spc=63;
    }

    else if(hpc==0)
    {
        hpc=15;
    }

    geo.rawnum=rawnum;
    geo.spc=spc;
    geo.hpc=hpc;

    geo.c=rawnum / (hpc * spc);
    geo.h=hpc;
    geo.s=spc;

    return geo;
}

int recalc_raw(geometry_t *g)
{
    g->rawnum=(g->c * g->hpc + g->h) * g->spc + (g->s -1);
    return g->rawnum;
}

fat_date_t decode_date(uint16_t d)
{
    fat_date_t da;

    da.year=(uint8_t) ((d & 0xFE00)>>9);
    da.month=(uint8_t) ((d & 0x01E0)>>5);
    da.day=(uint8_t) (d & 0x1F);

    return da;
}

fat_time_t decode_time(uint16_t t)
{
    fat_time_t ti;

    ti.hour=(uint8_t) ((t & 0xF800) >> 11);
    ti.minute=(uint8_t) ((t & 0x07F0) >> 5);
    ti.second=(uint8_t) (t & 0x001F);
    return ti;
}

uint8_t *read_sect(imgdesc_t *img, int sect_num)
{
    int offset;
    uint8_t *bytearr;

    offset=img->sect_size*sect_num;

    if(sect_num>img->num_sects)
    {
        return NULL;
    }

    bytearr=(uint8_t *) malloc(sizeof(uint8_t)*img->sect_size);

    fseek(img->f, offset, SEEK_SET);
    fread(bytearr, sizeof(uint8_t), img->sect_size, img->f);

    return bytearr;
}

uint8_t *read_sects(imgdesc_t *img, int start, int num)
{
    int offset;
    uint8_t *bytearr;

    offset=img->sect_size*start;

    if((start+num)>img->num_sects)
    {
        return NULL;
    }

    bytearr=(uint8_t *) malloc(sizeof(uint8_t)*img->sect_size*num);

    fseek(img->f, offset, SEEK_SET);
    fread(bytearr, sizeof(uint8_t), img->sect_size*num, img->f);

    return bytearr;
}

imgdesc_t openimg(const char *filename)
{
    imgdesc_t img;
    bootsect_t *boot;
    uint8_t *sectordata;
    int i;

    img.f=fopen(filename, "rb+");
    
    if(img.f==NULL)
    {
        img.fstype=0;
        img.num_sects=0;
        img.sect_size=0;
        img.bootsector=NULL;
        img.f=NULL;
	    return img;
    }

    /* Set some defaults for now: */
    img.sect_size=512;
    img.num_sects=flen(img.f)/img.sect_size;

    /* Try to determine a filesystem type: */
    sectordata=read_sect(&img, 0);
    boot=(bootsect_t*) sectordata;

    if(!strncmp(FAT12_STR, (const char*) boot->fstype_str, 8))
    {
        img.fstype=FAT12;
    }

    else if(!strncmp(FAT16_STR, (const char*) boot->fstype_str, 8))
    {
        img.fstype=FAT16;
    }

    else if(!strncmp(FAT32_STR, (const char*) boot->fstype_str, 8))
    {
        img.fstype=FAT32;
    }

    else
    {
        printf("ERROR: Could not determine image filesystem type!\n");
        printf("Defaulting to FAT16.\n");
        printf("Raw FS data:\n");
        for(i=0;i<8;i++)
        {
            printf("%c", boot->fstype_str[i]);
        }
        img.fstype=FAT16;
    }

    img.bootsector=boot;

    return img;
}

void closeimg(imgdesc_t *img)
{
    fclose(img->f);
    img->f=NULL;
    img->sect_size=0;
    img->num_sects=0;
    free(img->bootsector);
}

direntry_t *load_dir(imgdesc_t *img, int dir_start, int *num_entries)
{
    int dirsize;
    int numsects;
    uint8_t *raw_data;

    /* Start by calculating the size of the directory: */
    dirsize=img->bootsector->root_dir_size*sizeof(direntry_t);
    numsects=dirsize/img->sect_size;

    if((numsects*img->sect_size)<dirsize)
    {
        numsects++;
    }

    (*num_entries)=img->bootsector->root_dir_size;
    return (direntry_t*) read_sects(img, dir_start, numsects);
}

int get_data_start(imgdesc_t *img)
{
    bootsect_t *b;
    int dirsize;
    int numdirsects;

    b=img->bootsector;
    dirsize=b->root_dir_size*32;
    numdirsects=(dirsize+img->sect_size-1)/img->sect_size;

    /* TODO: Implement support for FAT32 cluster number entries. */
    return b->rsrvd_sects+b->num_fats*b->sects_per_fat+numdirsects;
}

int get_root_start(imgdesc_t *img)
{
    bootsect_t *b;

    b=img->bootsector;
    return b->sects_per_fat*b->num_fats+b->rsrvd_sects;
}

int isdir(direntry_t e)
{
    return e.attribs==0x10;
}

int islong(direntry_t e)
{
    return e.attribs==0xF0;
}

uint8_t *load_fat(imgdesc_t *img, int *fatl)
{
    bootsect_t *b;
    uint8_t *fatdata;
    int fatstart;
    int fatlen;
    
    b=img->bootsector;
    
    /* Determine where, exactly, the (first) FAT is: */
    fatstart=b->rsrvd_sects;
    fatlen=b->sects_per_fat;
    
    fatdata=read_sects(img, fatstart, fatlen);
    
    (*fatl)=fatlen*img->sect_size;
    return fatdata;
}

uint16_t lookup_12(uint8_t *fat, uint16_t curidx, int fatsize)
{
    int modsize;
    uint16_t *realarr;
    uint16_t modidx;
    uint16_t retval;
    
    modsize=fatsize>>1; /* Raw size should be 16 bit.*/
    if(curidx>=modsize || curidx==0x07FF || !curidx)
    {
	    return 0x7FF;
    }
    
    modidx=curidx+(curidx>>1);
    
    //realarr=(uint16_t*) (fat+modidx);
    //retval=realarr[0];
    retval = *(uint16_t*) &fat[modidx];
    
    if(modidx & 1) /* If the index is odd. */
    {
	    retval=retval>>4;
    }
    
    else
    {
	    retval=retval & 0x0FFF;
    }

    return retval;
}

uint16_t lookup_16(uint8_t *fat, uint16_t curidx, int fatsize)
{
    int modsize;
    uint16_t *realarr;
    
    modsize=fatsize>>1;
    
    if(curidx>=modsize || curidx==0xFFFF || !curidx)
    {
	    return 0xFFFF;
    }
    
    realarr=(uint16_t*) fat;
    
    return realarr[curidx];
}

uint32_t lookup_32(uint8_t *fat, uint32_t curidx, int fatsize)
{
    int modsize;
    uint32_t *realarr;
    
    modsize=fatsize>>2;
    
    if(curidx>=modsize || curidx==0xFFFFFFFF || !curidx)
    {
	    return 0xFFFFFFFF;
    }
    
    realarr=(uint32_t*) fat;
    
    return realarr[curidx];
}

uint32_t get_cluster_num(direntry_t e)
{
    uint32_t clstr;

    clstr=0;
    clstr=clstr|e.first_cluster_high;
    clstr=clstr<<16;
    clstr=clstr|e.first_cluster_low;

    return clstr;
}

int count_clusters(uint8_t *fat, int fatsize, direntry_t entry, int fstype)
{
    int numclusters;
    uint32_t clstr;

    clstr=get_cluster_num(entry);

    numclusters=0;

    if(fstype==FAT12)
    {
        while(clstr!=0x000007FF && clstr!=0)
        {
            clstr=(uint32_t) lookup_12(fat, (uint16_t) clstr, fatsize);
            numclusters++;
        }
    }

    else if(fstype==FAT16)
    {
        while(clstr!=0x0000FFFF && clstr!=0)
        {
            clstr=(uint32_t) lookup_16(fat, (uint16_t) clstr, fatsize);
            numclusters++;
        }
    }

    else if(fstype==FAT32)
    {
        while(clstr!=0xFFFFFFFF && clstr!=0)
        {
            clstr=lookup_32(fat, clstr, fatsize);
            numclusters++;
        }
    }

    return numclusters-1;
}

uint8_t *load_file(imgdesc_t *img, uint8_t *fat, int fatsize, direntry_t fileentry, int *filesize)
{
    uint32_t curclstr;
    uint16_t curclstr_shrt;
    uint8_t *retbuf;
    uint8_t *tmpbuf;
    int bufsize;
    int clstrct;
    int i, j;
    int offset;
    int ds_offset;

    curclstr=0;
    curclstr=curclstr|fileentry.first_cluster_high;
    curclstr=curclstr<<16;
    curclstr=curclstr|fileentry.first_cluster_low;

    clstrct=count_clusters(fat, fatsize, fileentry, img->fstype);
    bufsize=clstrct*img->sect_size;

    retbuf=(uint8_t*) malloc(sizeof(uint8_t)*bufsize);

    curclstr+=(uint32_t) get_data_start(img);
    curclstr-=2; /* Clusters start at 2. Don't ask why.*/
    offset=0;

    ds_offset=get_data_start(img)-2;

    printf("Loading: ");
    for(i=0;i<clstrct;i++)
    {
        tmpbuf=read_sect(img, curclstr);

        if(tmpbuf==NULL)
        {
            printf("ERROR: Could not read cluster %d\n", curclstr);
            free(retbuf);
            return NULL;
        }

        for(j=0;j<img->sect_size;j++)
        {
            retbuf[j+offset]=tmpbuf[j];
        }

        offset+=img->sect_size;
        free(tmpbuf);

        curclstr-=(uint32_t) ds_offset;

        if(img->fstype==FAT12)
        {
            curclstr=(uint32_t) lookup_12(fat, (uint16_t) curclstr, fatsize);
        }

        else if(img->fstype==FAT16)
        {
            curclstr=(uint32_t) lookup_16(fat, (uint16_t) curclstr, fatsize);
        }

        else if(img->fstype==FAT32)
        {
            curclstr=lookup_32(fat, curclstr, fatsize);
        }

        printf("%d+%d ", (get_data_start(img)-2), (int) curclstr);

        curclstr+=(uint32_t) ds_offset;
    }

    printf("\n");

    (*filesize)=bufsize;
    return retbuf;
}

direntry_t find(char *filename, direntry_t *dir, int direntries)
{
    direntry_t retentry;
    char *cmpptr;
    int i, j;
    unsigned char cmpv;
    int filenamelen;
    
    for(i=0;i<direntries;i++)
    {
        cmpptr=dir[i].filename;
        cmpv=0x00;

        filenamelen=strlen(filename);

        for(j=0;j<filenamelen;j++)
        {
            if(filename[j]=='.')
            {
                cmpptr=dir[i].ext;
            }

            else
            {
                /* As differences are found, cmpv becomes saturated with bits. */
                cmpv=cmpv | ((*cmpptr) ^ filename[j]);
                cmpptr++;
            }
        }

        if(!cmpv)
        {
            return dir[i];
        }
    }

    retentry.filename[0]=0x00;

    return retentry;
}

direntry_t *resolve_path(imgdesc_t *img, direntry_t *curdir, int curdirsize, int *founddirsize, char **path, int pathentries, uint8_t *fat, int fatsize)
{
    direntry_t *tmpdir;
    direntry_t foundval;
    int tmpdirlen;
    int i, j;

    printf("Path entries: %d\n", pathentries);

    tmpdir=curdir;
    tmpdirlen=curdirsize;

    /* A path is all directories except for (possibly) the last entry: */
    for(i=0;i<(pathentries-1);i++)
    {
        printf("Cur thing: %s\n", path[i]);
        foundval=find(path[i], tmpdir, tmpdirlen);

        if(foundval.filename[0]==0x00)
        {
            if(tmpdir!=curdir)
            {
                free(tmpdir);
            }

            return NULL;
        }

        if(tmpdir!=curdir)
        {
            free(tmpdir);
        }

        printf("Going into: ");
        for(j=0;j<11;j++)
        {
            printf("%c", foundval.filename[j]);
        }
        printf("\n");

        tmpdir=(direntry_t*) load_file(img, fat, fatsize, foundval, &tmpdirlen);
        printf("tmpdirlen=%d\n", tmpdirlen);
        tmpdirlen/=32;
    }

    (*founddirsize)=tmpdirlen;

    return tmpdir;
}

int write_sects(imgdesc_t *img, int start, uint8_t *contents, int contents_size)
{
    fseek(img->f, img->sect_size*start, SEEK_SET);
    return fwrite(contents, sizeof(uint8_t), contents_size, img->f)/img->sect_size;
}
