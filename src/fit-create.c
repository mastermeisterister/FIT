/* fit-create.c -- FAT disk image creation tool. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <getopt.h>

#include "common.h"
#include "fatimg.h"

geometry_t lookup_geometry(char *fmt)
{
    geometry_t g;
    g.rawnum=-1;

    if(!strcmp(fmt, "1.44M"))
    {
        return calculate(2880, 18, 2);
    }

    return g;
}

void genimg(char *filename, char *fmtstr, char *bitstr, char *disklabel, int lba, int spt, int hpc)
{
    bootsect_t *sect;
    imgdesc_t img;
    FILE *f;
    int tmp;
    int i;
    uint8_t *buf;
    geometry_t g;

    if(fmtstr==NULL)
    {
        if(lba==0)
        {
            printf("Could not calculate geometry without -n or -f.\n");
            return;
        }

        g=calculate(lba, spt, hpc);
        printf("Computed geometry:\n");
        printf("c: %d\nh: %d\ns: %d\n", g.c, g.h, g.s);
        printf("sects/cyl: %d\n", g.spc);
        printf("heads/cyl: %d\n", g.hpc);
    }

    else
    {
        g=lookup_geometry(fmtstr);

        if(g.rawnum==-1)
        {
            printf("Cannot generate geometry for %s.\n", fmtstr);
            return;
        }
    }

    /* Select a default based on the geometry.*/
    if(bitstr==NULL)
    {
        if(g.rawnum<0x0FFF)
        {
            img.fstype=FAT12;
        }

        else if(g.rawnum<0xFFFF)
        {
            img.fstype=FAT16;
        }
        
        else if(g.rawnum<0xFFFFFFFF)
        {
            img.fstype=FAT32;
        }

        else
        {
            printf("Requested image size too large.\n");
            return;
        }
    }

    else
    {
        if(!strcmp(bitstr, "12"))
        {
            img.fstype=FAT12;
        }

        else if(!strcmp(bitstr, "16"))
        {
            img.fstype=FAT16;
        }

        else
        {
            printf("Format: %s is unsupported. Defaulting to FAT12.\n", bitstr);
            img.fstype=FAT12;
        }
    }

    f=fopen(filename, "wb+");

    if(f==NULL)
    {
        printf("Couldn't open file: %s\n", filename);
    }

    /* Write some blank space: */
    printf("Writing %s...\n", filename);
    buf=(uint8_t *) calloc(512, sizeof(uint8_t));
    for(i=0;i<g.rawnum;i++)
    {
        fwrite(buf, sizeof(uint8_t), 512, f);
    }
    free(buf);

    rewind(f);

    sect=(bootsect_t*) malloc(sizeof(bootsect_t));

    sect->prefix[0]=0xEB; /* jmp short */
    sect->prefix[1]=0x00; /* Hang the computer (probably) */
    sect->prefix[2]=0x90; /* nop */

    strcpy(sect->oem_label, "FATIMGTL");

    /* TODO: Implement actual filesystem bit checking: */
    strcpy(sect->fstype_str, "FAT12   ");

    /* Zero out the boot code: */
    /* TODO: Allow users to specify their own bootcode files? */
    for(i=0;i<448;i++)
    {
        sect->bootcode[i]=0x00;
    }

    /* Set the boot signature: */
    sect->boot_signature=0xAA55;

    /* TODO: Make this make sense for more formats: */
    sect->rsrvd_sects=1;
    sect->num_fats=2;
    sect->sects_per_fat=9;
    sect->bytes_per_sect=512;
    sect->sects_per_clstr=1;
    sect->num_sectors=g.rawnum;
    if(g.rawnum>5760)
    {
        sect->media_desc=0xF8;
    }

    else
    {
        sect->media_desc=0xF0;
    }
    sect->root_dir_size=224;
    sect->sects_per_track=g.spc;
    sect->num_heads=g.hpc;
    sect->num_hidden=0;
    
    sect->signature=0x28;

    if(disklabel==NULL)
    {
        strcpy(sect->vol_label, "01234567");
    }

    else
    {
        tmp=strlen(disklabel);

        for(i=0;i<11;i++)
        {
            if(i>tmp)
            {
                sect->vol_label[i]=' ';
            }

            else
            {
                sect->vol_label[i]=disklabel[i];
            }
        }
    }

    /* Write out the boot sector: */
    fwrite(sect, sizeof(bootsect_t), 1, f);

    /* Write out the FATs: */
    buf=(uint8_t *) calloc(512*sect->sects_per_fat, sizeof(uint8_t));

    buf[0]=0xFF;
    buf[1]=0x0F;

    fwrite(buf, sizeof(uint8_t), 512*sect->sects_per_fat, f);
    fwrite(buf, sizeof(uint8_t), 512*sect->sects_per_fat, f);
    
    fclose(f);
    free(sect);
    free(buf);

    printf("Done.\n");
}

void print_usage()
{
    printf("Usage: fit-create <image file> [args]\n");
    printf("Create a FAT-formatted image file.\n\n");
    printf("\t-h      \tPrint this message.\n");
    printf("\t-f [opt]\tUse a built-in format. Supported formats include: 1.44M\n");
    printf("\t-n [val]\tSpecify a number of sectors.\n");
    printf("\t-t [val]\tSpecify the maximum number of sectors per track.\n");
    printf("\t-d [val]\tSpecify the maximum number of heads per cylinder.\n");
    printf("\t-s [opt]\tSpecify filesystem bits. Supported values: 12, 16\n");
    printf("\t-l [lbl]\tSpecify a disk label.\n");
    printf("Example: fit-create myimg.img -f 1.44M -s 12\n");
}

int main(int argc, char **argv)
{
    char *fmtstr;
    char *bitstr;
    char *fname;
    char *disklabel;
    int num, spt, hpc;

    int c;

    if(argc<2)
    {
        print_usage();
        return 0;
    }

    fname=argv[1];
    fmtstr=NULL;
    bitstr=NULL;
    disklabel=NULL;
    num=0;
    spt=0;
    hpc=0;

    while((c=getopt(argc, argv, "hf:s:l:n:t:d:"))!=-1)
    {
        switch(c)
        {
            case 'h':
                print_usage();
                return 0;
            case 'f':
                fmtstr=optarg;
                break;
            case 's':
                bitstr=optarg;
                break;
            case 'l':
                disklabel=optarg;
                break;
            case 'n':
                sscanf(optarg, "%d", &num);
                break;
            case 't':
                sscanf(optarg, "%d", &spt);
                break;
            case 'd':
                sscanf(optarg, "%d", &hpc);
                break;
            case '?':
                print_usage();
                return 1;
        }
    }

    genimg(fname, fmtstr, bitstr, disklabel, num, spt, hpc);
}
