/* fatimg.h -- Definitions for FAT image files. */

#ifndef FATIMG_H
#define FATIMG_H
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "common.h"

/* Defines a structure for FAT12 and FAT16 boot sectors.*/
#pragma pack(push, 1)
typedef struct
{
    uint8_t  prefix[3];
    uint8_t  oem_label[8];
    uint16_t bytes_per_sect;
    uint8_t  sects_per_clstr;
    uint16_t rsrvd_sects;
    uint8_t  num_fats;
    uint16_t root_dir_size;
    uint16_t num_sectors;
    uint8_t  media_desc;
    uint16_t sects_per_fat;
    uint16_t sects_per_track;
    uint16_t num_heads;
    uint32_t num_hidden;
    uint32_t large_num_sectors;
    uint8_t  drive_number;
    uint8_t  flgs;
    uint8_t  signature;
    uint32_t volid_serial;
    uint8_t  vol_label[11];
    uint8_t  fstype_str[8];
    uint8_t  bootcode[448];
    uint16_t boot_signature;
} bootsect_t;
#pragma pack(pop)

/* Defines a structure for FAT12 and FAT16 directory entries: */
#pragma pack(push, 1)
typedef struct
{
    uint8_t  filename[8];
    uint8_t  ext[3];
    uint8_t  attribs;
    uint8_t  rsrvd;
    uint8_t  creation_time_fraction;
    uint16_t creation_time;
    uint16_t creation_date;
    uint16_t last_accessed;
    uint16_t first_cluster_high;
    uint16_t last_mod_time;
    uint16_t last_mod_date;
    uint16_t first_cluster_low;
    uint32_t file_size;
} direntry_t;
#pragma pack(pop)

/* Defines a long directory entry: */
#pragma pack(push, 1)
typedef struct
{
    uint8_t  entry_order;
    uint16_t chars[5];
    uint8_t  attribs;      /* Should == 0x0F*/
    uint8_t  long_entry_type;
    uint8_t  chksum;
    uint16_t chars2[6];
    uint16_t blank;
    uint16_t chars3[2];
} longdirentry_t;
#pragma pack(pop)

typedef struct
{
    int fstype;
    int num_sects;
    int sect_size;
    bootsect_t *bootsector;
    FILE *f;
} imgdesc_t;

typedef struct
{
    uint8_t year, month, day; /* Year is an offset from 1980, when time began. */
} fat_date_t;

typedef struct
{
    uint8_t hour, minute, second;
} fat_time_t;

typedef struct
{
    int rawnum;
    int c, h, s;
    int spc;
    int hpc;
} geometry_t;

#define FAT12 1
#define FAT12_STR "FAT12   "
#define FAT16 2
#define FAT16_STR "FAT16   "
#define FAT32 3
#define FAT32_STR "FAT32   "

/* Computes the geometry for a target disk. */
geometry_t calculate(int rawnum, int spc, int hpc);

/* Recomputes the raw number of sectors from a geometry structure. */
int recalc_raw(geometry_t *g);

/* Decodes a date field.
 * Returns: fat_date_t representing the date.
 */
fat_date_t decode_date(uint16_t d);

/* Decodes a time field.
 * Returns: fat_time_t representing the time.
 */
fat_time_t decode_time(uint16_t t);

/* Reads a "sector" from the target image.
 * img -- The image to read from.
 * sect_num -- the sector to read from.
 * Returns: a malloc'd array of data or NULL on error.
 */
uint8_t *read_sect(imgdesc_t *img, int sect_num);

/* Reads a number of "sectors" sequentially from the target image.
 * img -- The image to read from.
 * start -- The first "sector"
 * num -- The number of "sectors" to read.
 * Returns: a malloc'd array of data or NULL on error.
 */
uint8_t *read_sects(imgdesc_t *img, int start, int num);

int write_sects(imgdesc_t *img, int start, uint8_t *contents, int contents_size);

imgdesc_t openimg(const char *filename);
void closeimg(imgdesc_t *img);

direntry_t *load_dir(imgdesc_t *img, int dir_start, int *num_entries);
int get_root_start(imgdesc_t *img);
int get_data_start(imgdesc_t *img);
int isdir(direntry_t e);

uint8_t *load_fat(imgdesc_t *img, int *fatl);

uint16_t lookup_12(uint8_t *fat, uint16_t curidx, int fatsize);
uint16_t lookup_16(uint8_t *fat, uint16_t curidx, int fatsize);
uint32_t lookup_32(uint8_t *fat, uint32_t curidx, int fatsize);

uint32_t get_cluster_num(direntry_t e);

int count_clusters(uint8_t *fat, int fatsize, direntry_t entry, int fstype);
uint8_t *load_file(imgdesc_t *img, uint8_t *fat, int fatsize, direntry_t fileentry, int *filesize);

direntry_t *resolve_path(imgdesc_t *img, direntry_t *curdir, int curdirsize, int *founddirsize, char **path, int pathentries, uint8_t *fat, int fatsize);
direntry_t find(char *filename, direntry_t *dir, int direntries);
#endif
