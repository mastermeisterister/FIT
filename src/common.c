#include "common.h"

int flen(FILE *f)
{
    fseek(f, 0, SEEK_END);
    return ftell(f);
}

char **tokenize(char *src, char *tokstr, int *numtoks)
{
    int tmpnum;
    char *tmpstr;
    char **strtoks;
    int i, j;
    int srclen;
    int chklen;
    
    /* Get the number of string tokens there are: */
    srclen=strlen(src);
    chklen=strlen(tokstr);
    tmpnum=0;
    
    for(i=0;i<=srclen;i++)
    {
        for(j=0;j<chklen;j++)
        {
            if(src[i]==tokstr[j])
            {
                tmpnum++;
                break;
            }
        }
    }
    
    strtoks=(char**) malloc(sizeof(char*)*tmpnum);
    
    strtoks[0]=strtok(src, tokstr);
    
    for(i=1;i<tmpnum;i++)
    {
        strtoks[i]=strtok(NULL, tokstr);
    }

    (*numtoks)=tmpnum;
    
    return strtoks;
}
