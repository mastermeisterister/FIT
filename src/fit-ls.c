/* fit-ls.c -- Directory listing tool for FAT disk images.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <getopt.h>

#include "common.h"
#include "fatimg.h"

void dolist(char *filename, char *path, int longflag)
{
    imgdesc_t img;
    uint8_t *fat;
    direntry_t *dir;
    direntry_t *rootdir;
    char *pathstr;
    char **pathtoks;
    int numpathtoks;
    int numdirentries, numfatentries;
    int i;
    int j;
    int founddirsize;
    
    img=openimg((const char*) filename);
    
    if(img.f==NULL)
    {
	    printf("Couldn't find file: %s\n", filename);
	    return;
    }
    
    fat=load_fat(&img, &numfatentries);
    rootdir=load_dir(&img, get_root_start(&img), &numdirentries);

    if(path!=NULL)
    {
        pathstr=(char*) malloc(sizeof(char)*strlen(path)+1);
        strcpy(pathstr, path);
        pathtoks=tokenize(pathstr, "/", &numpathtoks);
        dir=resolve_path(&img, rootdir, numdirentries, &founddirsize, pathtoks, numpathtoks, fat, numfatentries);

        if(dir==NULL)
        {
            printf("Invalid path: %s\n", path);
            free(fat);
            free(rootdir);
            closeimg(&img);
            return;
        }

        numdirentries=founddirsize;
        printf("Number of directory entries: %d\n", numdirentries);
    }
    
    else
    {
        dir=rootdir;
    }
    
    for(i=0;i<numdirentries;i++)
    {
	    if(dir[i].filename[0]!=0x00 && dir[i].attribs!=0x0F && dir[i].filename[0]!=0xE5)
	    {
	        for(j=0;j<8;j++)
	        {
		        printf("%c", dir[i].filename[j]);
	        }
	        
	        if(dir[i].ext[0]!=' ')
	        {
		        printf(".");
	        }
	        
	        else
	        {
		        printf(" ");
	        }
	        
	        for(j=0;j<3;j++)
	        {
		        printf("%c", dir[i].ext[j]);
	        }
	        
	        printf(" | ");
	        
	        if(dir[i].attribs==0x10)
	        {
		        printf("DIR");
	        }

            else if(dir[i].attribs==0x01)
            {
                printf("RO");
            }

            else if(dir[i].attribs==0x02)
            {
                printf("HDDN");
            }

            else if(dir[i].attribs==0x04)
            {
                printf("SYS");
            }
	        
	        printf("\n");
	    }
    }
    
    free(fat);

    if(dir!=rootdir)
    {
        free(rootdir);
    }

    free(dir);
    closeimg(&img);
}

void print_usage()
{
    printf("Usage: fit-ls <image-file> [args]\n");
    printf("List files in a disk image.\n");
    printf("\nWhere [args] is one of the following:\n");
    printf("\t-p\tList files in a specific path.\n");
    printf("\t-l\tDo a long directory listing.\n");
}

int main(int argc, char **argv)
{
    int longflag;
    int i;
    char *path;
    char *fname;
    int chr;
    
    if(argc<2)
    {
        print_usage();
        return 0;
    }
    
    fname=argv[1];
    path=NULL;

    while((chr=getopt(argc, argv, "hlp:")) != -1)
    {
	    switch(chr)
	    {
	        case 'h':
		        print_usage();
		        return 0;
	        case 'l':
		        longflag=1;
		        break;
	        case 'p':
		        path=optarg;
                printf("Path: %s\n", path);
		        break;
	        case '?':
		        if(optopt=='p')
		        {
		            return 1;
		        }
	    }
    }
    
    dolist(fname, path, longflag);
    return 0;
}
