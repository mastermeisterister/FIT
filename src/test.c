// Test to use fatimg.

#include <stdio.h>
#include "fatimg.h"

int main()
{
    uint16_t d, t;
    fat_time_t time;
    fat_date_t date;
    imgdesc_t img;
    int numdirentries;
    direntry_t *entries;
    int i, j;
    uint8_t *fat;
    int fatlen;
    uint16_t curclstr;

    t=0x4868;
    d=0x486F;

    time=decode_time(t);
    date=decode_date(d);

    printf("Time: %d:%02d and %d seconds\n", time.hour, time.minute, time.second);
    printf("Date: %d-%d-%d\n", 1980+date.year, date.month, date.day);

    img=openimg("test.img");
    printf("Directory area start: %d\n", get_root_start(&img));
    printf("Data area start: %d\n", get_data_start(&img));
    entries=load_dir(&img, get_root_start(&img), &numdirentries);
    printf("Number of directory entries: %d\n", numdirentries);
    fat=load_fat(&img, &fatlen);
    
    for(i=0;i<10;i++)
    {
        printf("Filename: ");
        for(j=0;j<11;j++)
        {
            printf("%c", entries[i].filename[j]);
        }
        printf("\n");
	    printf("Clusters: \n");
	
	    curclstr=entries[i].first_cluster_low;
	
	    while(curclstr!=0x7FF)
	    {
                printf("%d ", curclstr);
                curclstr=lookup_12(fat, curclstr, fatlen);
	    }
	
	    printf("\n");
    }
    free(fat);
    free(entries);
    closeimg(&img);

    return 0;
}
