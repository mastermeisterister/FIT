/* commontest.c -- A quick test program for common.h */
#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int i;
    char *str;
    char **pathtoks;
    int numpathtoks;
    
    str=(char*) malloc(sizeof(char)*strlen("/var/www/mything")+1);
    strcpy(str, "/var/www/mything");
    
    pathtoks=tokenize(str, "/", &numpathtoks);
    
    printf("Number of tokens: %d\n", numpathtoks);
    
    for(i=0;i<numpathtoks;i++)
    {
        printf("%s\n", pathtoks[i]);
    }
    
    free(pathtoks);
    free(str);
    
    return 0;
}